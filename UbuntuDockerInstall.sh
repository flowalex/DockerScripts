#bin/sh

#linux

#Installing Docker Community Edition on Ubuntu 18.04


echo "This script will install Docker Community Edition for Ubuntu 18.04"
echo "1. Set up the repository"
sudo sudo apt-get update

#Set up the Docker CE repository on Ubuntu. The lsb_release -cs sub-command prints the name of your Ubuntu version, like xenial or trusty.
sudo apt-get install apt-transport-https ca-certificates curl gnupg software-properties-common -y

sudo apt-get -y install \
  apt-transport-https \
  ca-certificates \
  curl

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

sudo add-apt-repository \
       "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
       $(lsb_release -cs) \
       stable"

sudo apt-get update


echo "Step 2. Get Docker CE"

#Install the latest version of Docker CE on Ubuntu:

sudo apt-get -y install docker-ce
echo "Step 3. Test your Docker CE installation"
echo "Test your installation:"

sudo docker run hello-world
#Next stepsecho "The script wil now create a user group called docker and add the current user to that group"
sudo groupadd docker

sudo usermod -aG docker $USER
sleep 5
echo "It will now test the Docker install and make sure it is enabled"
docker run hello-world

sudo systemctl enable docker

sudo systemctl status docker

