#bin/sh

#linux

#Installing Docker Community Edition on Ubuntu 16.04

echo "This script will install Docker Community Edition for Ubuntu 16.04"
echo "1. Set up the repository"
sudo sudo yum update

#Set up the Docker CE repository on Ubuntu. The lsb_release -cs sub-command prints the name of your Ubuntu version, like xenial or trusty.

sudo dnf -y install dnf-plugins-core

sudo dnf config-manager \
    --add-repo \
    https://download.docker.com/linux/fedora/docker-ce.repo

   sudo dnf makecache fast 


sudo yum update


echo "Step 2. Get Docker CE"

#Install the latest version of Docker CE on Ubuntu:

sudo dnf -y install docker-ce
echo "Step 3. Test your Docker CE installation"
echo "Test your installation:"


sudo systemctl start docker


#Next stepsecho "The script wil now create a user group called docker and add the current user to that group"
sudo groupadd docker

sudo usermod -aG docker $USER
sleep 5
echo "It will now test the Docker install and make sure it is enabled"
sudo docker run hello-world

echo "if you want a ui for the docker container, I recomend simple-docker-ui from github user felixgborrego
at https://github.com/felixgborrego/simple-docker-ui  They don't have a fedora package out of the box, but
they have an electron app and one probally could compile the
"