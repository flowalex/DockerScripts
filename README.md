# DockerScripts  

## This repo is no-longer being updated, please go to [docker-utils](https://gitlab.com/flowalex/docker-utils) to see all new docker scripts container configs

Scripts for installing docker, as well as Drupal and Drupal commerce in docker  

I have tested the Drupal install scripts as well as the Ubuntu install script, Fedora script testing comming soon.  

I have a plan to make the drupal ones work for Windows as well in the future, but I believe the Drupal scripts already work on computers running OSX  

These scripts were created for a final project, with training documentation comming later
