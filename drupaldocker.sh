#bin/sh

#Docker

#Docker Drupal Script

echo "Script will install Drupal using a Docker Container"

#The following command is setting up the database, please remeber to change the database name which currently is set as MYSQL-NAME and the root password which is MYSQL-PASSWORD
#docker run --name MYSQL-NAME -e MYSQL_ROOT_PASSWORD=MYSQL-PASSWORD -d mysql:latest
echo "Please enter a name for your database"
read MYSQLNAME
echo "Please enter a password for your database"
read MYSQLPASSWORD
docker run --name $MYSQLNAME -e MYSQL_ROOT_PASSWORD=$MYSQLPASSWORD -d mysql:latest

echo "Please enter a name for your Drupal Container"
read somedrupal

docker run --name $somedrupal --link $MYSQLNAME:mysql -p 8080:80 -e MYSQL_USER=root -e MYSQL_PASSWORD=$MYSQLPASSWORD -d drupal

clear
echo "Please record this information as it will be used later for installing the Drupal Site"
echo "Your site Docker Container iformation is:
MYSQL Container Name: " $MYSQLNAME
echo "MYSQL USERNAME: root"
echo "MYSQLPASSWORD: " $MYSQLPASSWORD
echo "Drupal Container Name: " $somedrupal

sleep 15

echo "When setting up the database for the drupal site enter the following information:
Database name: drupal
Database username: root
Database Host:" $MYSQLNAME
"Database password: " $MYSQLPASSWORD